package ai.deepsea.exercise.akotsiras.controller;

import ai.deepsea.exercise.akotsiras.views.HostRequestURIs;
import ai.deepsea.exercise.akotsiras.views.InitDatabaseResults;
import ai.deepsea.exercise.akotsiras.views.Percentage;
import ai.deepsea.exercise.akotsiras.service.HttpLogService;
import ai.deepsea.exercise.akotsiras.service.InitDatabaseService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping(path="api/v1/httplog")
public class HttpLogController {

    LocalDateTime initializedDateTime ;
    private static final Logger log = LoggerFactory.getLogger(HttpLogController.class);

    @Autowired HttpLogService httpLogService;
    @Autowired InitDatabaseService initDatabaseService;

    @PostConstruct
    void init() {
        initializedDateTime = LocalDateTime.now();
    }

    /**
     * 1. Το top10 των σελίδων που ζήτησαν οι χρήστες, και τον αριθμό των requests για την κάθε σελίδα.
     */
    @GetMapping(path = "requestCountByURITop10")
    public Collection<Object> getRequestCountByURITop10() {
        List<Object> objectList = httpLogService.getRequestCountByURITop10();
        return objectList;
    }

    /**
     * 2. Το ποσοστό των επιτυχημένων requests (οτιδήποτε με status 2xx και 3xx).
     */
    @GetMapping(path = "percentageOfSuccessfulRequests")
    public Percentage getPercentageOfSuccessfulRequests() {
        Percentage percentage = httpLogService.getPercentageOfSuccessfulRequests();
        return percentage;
    }

    /**
     * 3. Το ποσοστό των ανεπιτυχών requests (οτιδήποτε με status διαφορετικό από 2xx και 3xx).
     */
    @GetMapping(path = "percentageOfUnSuccessfulRequests")
    public Percentage getPercentageOfUnSuccessfulRequests() {
        Percentage percentage = httpLogService.getPercentageOfUnSuccessfulRequests();
        return percentage;
    }

    /**
     * 4. Το top10 των ανεπιτυχών requests (χωρις count)
     */
    @GetMapping(path = "unsuccesfulRequestsURITop10")
    public Collection<Object> getUnsuccesfulRequestsURITop10() {
        List<Object> objectList = httpLogService.getUnsuccesfulRequestsURITop10();
        return objectList;
    }


    /**
     * 5. Το top10 των hosts που έχουν κάνει τα περισσότερα requests, εμφανίζοντας και
     *    την IP διεύθυνση/domain και τον αριθμό των requests που έχουν γίνει.
     */
    @GetMapping(path = "requestCountByHostTop10")
    public Collection<Object> getRequestCountByHostTop10() {
        List<Object> objectList = httpLogService.getRequestCountByHostTop10();
        return objectList;
    }

    /**
     * 6. Για κάθε ένα από τους top10 hosts, εμφάνισε τις top5 σελίδες που ζήτησε και τον αριθμό των requests για καθεμία.
     */
    @GetMapping(path = "requestCountByHostTop10AndURITop5")
    public List<HostRequestURIs> getRequestCountByHostTop10AndURITop5() {
        List<HostRequestURIs> hostRequestURIsList = httpLogService.getRequestCountByHostTop10AndURITop5();
        return hostRequestURIsList;
    }

    /**
     *  Special call that populates the HTTP_LOG and HTTP_LOG_FAILED tables from the Htttp Log File
     *  Will first TRUNCATE the tables and the populate them with data
     */
    @GetMapping(path = "initDatabase")
    public InitDatabaseResults initDatabase() throws IOException {
        InitDatabaseResults initDatabaseResults = initDatabaseService.populateDatabase();
        return initDatabaseResults;
    }


    public LocalDateTime getInitializedDateTime() {
        return initializedDateTime;
    }

}
