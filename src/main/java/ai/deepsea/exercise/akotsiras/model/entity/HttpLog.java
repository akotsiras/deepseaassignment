package ai.deepsea.exercise.akotsiras.model.entity;

import ai.deepsea.exercise.akotsiras.helpers.HttpLogLine;

import javax.persistence.*;

import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Date;

import static javax.persistence.GenerationType.SEQUENCE;

@Entity(name = "HttpLog")
@Table(name = "HTTP_LOG",
        indexes = @Index(name = "idx_http_log_status_class", columnList = "statusClass") // Not sure about that, needs extra work to see if it really helps
)
public class HttpLog {

    public enum STATUS_CLASS {
        SUCCESS, REDIRECTION, OTHER
    }

    @Id
    @SequenceGenerator(
            name = "seq_http_log",
            sequenceName = "seq_http_log",
            allocationSize = 1, initialValue = 1
    )
    @GeneratedValue(
            strategy = SEQUENCE,
            generator = "seq_http_log"
    )

    private Long id;
    private Long lineNumber;
    private String host;
    private Date requestTimestamp;
    private String uri;
    private Integer status;
    @Enumerated(EnumType.STRING)
    private STATUS_CLASS statusClass;
    private String bytes;
    private Date dateCreated;

    public HttpLog() {
    }

    public HttpLog(HttpLogLine httpLogLine) {
        this.lineNumber = httpLogLine.getLineNumber();
        this.host = httpLogLine.getClientHost();
        this.requestTimestamp = new Date(httpLogLine.getRequestepoch());
        this.uri = httpLogLine.getClientRequest();
        this.status = httpLogLine.getHttpStatusCode();
        this.bytes = httpLogLine.getNumOfBytes();
        if(this.status >= 200 && this.status < 300)
            this.statusClass = STATUS_CLASS.SUCCESS;
        else if(this.status >= 300 && this.status < 400)
            this.statusClass = STATUS_CLASS.REDIRECTION;
        else
            this.statusClass = STATUS_CLASS.OTHER;
        this.dateCreated = Calendar.getInstance().getTime();
    }

    public Long getId() {
        return id;
    }

    public Long getLineNumber() {
        return lineNumber;
    }

    public void setLineNumber(Long lineNumber) {
        this.lineNumber = lineNumber;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public Date getRequestTimestamp() {
        return requestTimestamp;
    }

    public void setRequestTimestamp(Date requestTimestamp) {
        this.requestTimestamp = requestTimestamp;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getBytes() {
        return bytes;
    }

    public void setBytes(String bytes) {
        this.bytes = bytes;
    }

    public STATUS_CLASS getStatusClass() {
        return statusClass;
    }

    public void setStatusClass(STATUS_CLASS statusClass) {
        this.statusClass = statusClass;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    @Override
    public String toString() {
        return "HttpLog{" +
                "id=" + id +
                ", line=" + lineNumber +
                ", host='" + host + '\'' +
                ", requestTimestamp=" + requestTimestamp +
                ", uri='" + uri + '\'' +
                ", status=" + status +
                ", bytes='" + bytes + '\'' +
                '}';
    }
}
