package ai.deepsea.exercise.akotsiras.model.repository;

import java.util.List;

public interface HttpLogRepositoryCustom {

    List<Object> successfulAndTotalRequestCount();
    List<Object> unsuccessfulAndTotalRequestCount();
    List<Object> requestCountByHostAndURITop5(String host);

}
