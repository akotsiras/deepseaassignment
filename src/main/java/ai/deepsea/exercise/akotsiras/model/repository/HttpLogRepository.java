package ai.deepsea.exercise.akotsiras.model.repository;

import ai.deepsea.exercise.akotsiras.model.entity.HttpLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface HttpLogRepository extends JpaRepository<HttpLog, Long>, HttpLogRepositoryCustom {

    /**
     *   1. Το top10 των σελίδων που ζήτησαν οι χρήστες, και τον αριθμό των requests για την κάθε σελίδα.
     */
    @Query(
            value = "select uri, count(*) as number_of_requests from http_log group by uri order by number_of_requests desc limit 10",
            nativeQuery = true
    )
    List<Object> requestCountByURITop10();



    /*
        4. Το top10 των ανεπιτυχών requests (χωρις count)

        select z.uri from
         ( select uri, count(*) as number_of_requests
           from http_log where status_class = 'OTHER'
           group by uri
           order by number_of_requests desc limit 10
         ) as z;
     */
    @Query(
            value = "select z.uri from \n" +
                    " ( select uri, count(*) as number_of_requests \n" +
                    "   from http_log where status_class = 'OTHER' \n" +
                    "   group by uri \n" +
                    "   order by number_of_requests desc limit 10\n" +
                    " ) as z;",
            nativeQuery = true
    )
    List<Object> unsuccesfulRequestsURITop10();


    /**
     * 5. Το top10 των hosts που έχουν κάνει τα περισσότερα requests, εμφανίζοντας και
     *    την IP διεύθυνση/domain και τον αριθμό των requests που έχουν γίνει.
     */
    @Query(
            value = "select host, count(*) as number_of_requests from http_log group by host order by number_of_requests desc limit 10",
            nativeQuery = true
    )
    List<Object> requestCountByHostTop10();


    @Modifying
    @Query(
            value = "truncate table http_log",
            nativeQuery = true
    )
    void truncateTableHttpLog();

    @Modifying
    @Query(
            value = "truncate table http_log_failed",
            nativeQuery = true
    )
    void truncateTableHttpLogFailed();





    /*   SQL statement bellow refuses to work !!
        select * from (
            select
            ( select count(*) from http_log where status_class = 'REDIRECTION'),
            ( select count(*) from http_log)
        ) as z ;
     */
    /*
    @Query(
            value = "select * from (\n" +
                    "\tselect \n" +
                    "\t( select count(*) from http_log where status_class in ('REDIRECTION') " +
                    ") as z",
            nativeQuery = true
    )
    List<Object> percentageOfSuccessfulRequests();

     */



}
