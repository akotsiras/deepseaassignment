package ai.deepsea.exercise.akotsiras.model.repository;

import ai.deepsea.exercise.akotsiras.model.entity.HttpLogFailed;
import org.springframework.data.jpa.repository.JpaRepository;

public interface HttpLogFailedRepository extends JpaRepository<HttpLogFailed, Long> {


}
