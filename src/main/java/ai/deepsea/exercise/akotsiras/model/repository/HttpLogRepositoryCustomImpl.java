package ai.deepsea.exercise.akotsiras.model.repository;

import org.springframework.lang.Nullable;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

public class HttpLogRepositoryCustomImpl implements HttpLogRepositoryCustom {

    @PersistenceContext EntityManager em;

    @Override
    public List<Object> successfulAndTotalRequestCount() {
        Query query = em.createNativeQuery(
            "select successful, total from (\n" +
                    "\tselect \n" +
                    "\t( select count(*) from http_log where status_class in ('SUCCESS','REDIRECTION') ) as successful,\n" +
                    "\t( select count(*) from http_log) as total\n" +
                    ") as z"
        );
        List<Object> l = query.getResultList();
        return l;
    }

    @Override
    public List<Object> unsuccessfulAndTotalRequestCount() {
        Query query = em.createNativeQuery(
                "select unsuccessful, total from (\n" +
                        "\tselect \n" +
                        "\t( select count(*) from http_log where status_class in ('OTHER')) as unsuccessful,\n" +
                        "\t( select count(*) from http_log) as total\n" +
                        ") as z"
        );
        List<Object> l = query.getResultList();
        return l;
    }

    /**
     *   6. Για ένα host, εμφάνισε τις top5 σελίδες που ζήτησε και τον αριθμό των requests για καθε μία.
     */
    @Override
    public List<Object> requestCountByHostAndURITop5(String host) {
        Query query = em.createNativeQuery(
                "select host, uri, count(*) as request_count \n" +
                        "from http_log hl \n" +
                        "where hl.host = ?1 \n" +
                        "group by host, uri order by request_count desc limit 5; "
        );
        query.setParameter(1, host);
        List<Object> l = query.getResultList();
        return l;
    }


}
