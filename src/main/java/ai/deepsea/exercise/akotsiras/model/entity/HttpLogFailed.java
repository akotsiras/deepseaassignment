package ai.deepsea.exercise.akotsiras.model.entity;

import ai.deepsea.exercise.akotsiras.helpers.HttpLogLine;

import javax.persistence.*;
import java.util.Calendar;
import java.util.Date;
import java.util.stream.Collectors;

import static javax.persistence.GenerationType.SEQUENCE;

@Entity(name = "HTTP_LOG_FAILED")
public class HttpLogFailed {

    @Id
    @SequenceGenerator(
            name = "seq_http_log_failed",
            sequenceName = "seq_http_log_failed",
            allocationSize = 1, initialValue = 1
    )
    @GeneratedValue(
            strategy = SEQUENCE,
            generator = "seq_http_log_failed"
    )

    private Long id;
    private Long lineNumber;
    @Column(length = 500)
    private String line;
    @Column(length = 500)
    private String errors;
    private Date dateCreated;

    public HttpLogFailed(HttpLogLine httpLogLine) {
        this.lineNumber = httpLogLine.getLineNumber();
        this.line = httpLogLine.getLine();
        this.errors = httpLogLine.getErrors().stream().collect(Collectors.joining(" | "));
        this.dateCreated = Calendar.getInstance().getTime();
    }

    public Long getId() {
        return id;
    }

    public Long getLineNumber() {
        return lineNumber;
    }

    public void setLineNumber(Long lineNumber) {
        this.lineNumber = lineNumber;
    }

    public String getLine() {
        return line;
    }

    public void setLine(String line) {
        this.line = line;
    }

    public String getErrors() {
        return errors;
    }

    public void setErrors(String errors) {
        this.errors = errors;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }
}
