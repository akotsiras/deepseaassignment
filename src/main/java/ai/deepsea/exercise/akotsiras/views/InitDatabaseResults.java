package ai.deepsea.exercise.akotsiras.views;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class InitDatabaseResults {

    private int logLinesCountSuccess ;
    private int logLinesCountUnparseable ;
    private long executionTimeSeconds;

    public InitDatabaseResults() {  }

    @JsonIgnore
    public int incLogLinesCountSuccess() {
        return logLinesCountSuccess += 1;
    }

    @JsonIgnore
    public int incLogLinesCountUnparseable() {
        return logLinesCountUnparseable += 1;
    }

    public int getLogLinesCountSuccess() {
        return logLinesCountSuccess;
    }

    public void setLogLinesCountSuccess(int logLinesCountSuccess) {
        this.logLinesCountSuccess = logLinesCountSuccess;
    }

    public int getLogLinesCountUnparseable() {
        return logLinesCountUnparseable;
    }

    public void setLogLinesCountUnparseable(int logLinesCountUnparseable) {
        this.logLinesCountUnparseable = logLinesCountUnparseable;
    }

    public int getLogLinesTotal() {
        return logLinesCountSuccess + logLinesCountUnparseable;
    }

    public long getExecutionTimeSeconds() {
        return executionTimeSeconds;
    }

    public void setExecutionTimeSeconds(long executionTimeSeconds) {
        this.executionTimeSeconds = executionTimeSeconds;
    }
}
