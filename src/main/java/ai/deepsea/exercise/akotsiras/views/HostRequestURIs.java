package ai.deepsea.exercise.akotsiras.views;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Used in No 6 REST Call
 * 6.) Για κάθε ένα από τους top10 hosts, εμφάνισε τις top5 σελίδες που ζήτησε και τον αριθμό των requests για καθεμία.
 */
public class HostRequestURIs {
    final String host;
    BigInteger hostRequestCount;

    Collection<RequestUri> requestUris = new ArrayList<>();

    public HostRequestURIs(String host) {
        this.host = host;
    }

    public HostRequestURIs(String host, BigInteger hostRequestCount) {
        this.host = host;
        this.hostRequestCount = hostRequestCount;
    }

    public String getHost() {
        return host;
    }

    public BigInteger getHostRequestCount() {
        return hostRequestCount;
    }

    @JsonProperty("top5RequestURIs")
    public Collection<RequestUri> getRequestUris() {
        return requestUris;
    }

    public void addRequestUri(String uri, BigInteger count) {
        this.requestUris.add(new RequestUri(uri, count)) ;
    }

    static class RequestUri {
        private String uri;
        private BigInteger requestCount;

        public RequestUri() { }

        public RequestUri(String uri, BigInteger requestCount) {
            this.uri = uri;
            this.requestCount = requestCount;
        }

        public String getUri() {
            return uri;
        }

        public void setUri(String uri) {
            this.uri = uri;
        }

        public BigInteger getRequestCount() {
            return requestCount;
        }

        public void setRequestCount(BigInteger requestCount) {
            this.requestCount = requestCount;
        }

        @Override
        public String toString() {
            return "RequestUri{" +
                    "uri='" + uri + '\'' +
                    ", requestCount=" + requestCount +
                    '}';
        }
    }

    @Override
    public String toString() {
        return "HostRequestURIs{" +
                "host='" + host + '\'' +
                ", requestUris=" + requestUris +
                '}';
    }
}
