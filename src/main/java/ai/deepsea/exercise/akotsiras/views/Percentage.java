package ai.deepsea.exercise.akotsiras.views;

public class Percentage {

    private String name;
    private String percentage;

    public Percentage() {
    }

    public Percentage(String name, String percentage) {
        this.name = name;
        this.percentage = percentage;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPercentage() {
        return percentage;
    }

    public void setPercentage(String percentage) {
        this.percentage = percentage;
    }
}
