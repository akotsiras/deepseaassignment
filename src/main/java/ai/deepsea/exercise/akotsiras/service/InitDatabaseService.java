package ai.deepsea.exercise.akotsiras.service;

import ai.deepsea.exercise.akotsiras.model.repository.HttpLogRepository;
import ai.deepsea.exercise.akotsiras.views.InitDatabaseResults;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.io.IOException;

/**
 * @Author Alexandros.Kotsiras
 * @Date 9/11/2021 8:24 μμ
 */
@Service
public class InitDatabaseService {

    Logger log = LoggerFactory.getLogger(InitDatabaseService.class);

    @Autowired HttpLogRepository httpLogRepository;

    @Value("classpath:new_final_final_01.log")
    Resource resourceFile;

    @Autowired
    HttpLogFileReaderService httpLogFileReaderService;

    @Transactional(value = Transactional.TxType.REQUIRED)
    public InitDatabaseResults populateDatabase() throws IOException {
        log.info("*********  InitDatabaseService START ***********");
        String httpLogFilePath = resourceFile.getFile().getAbsolutePath();
        log.info("httpLogFilePath = '" + httpLogFilePath + "'");

        // First TRUNCATE the HTTP_LOG and HTTP_LOG_FAILED tables just in case they were not recreated from scratch upon Application startup
        httpLogRepository.truncateTableHttpLog();
        httpLogRepository.truncateTableHttpLogFailed();
        httpLogFileReaderService.setHttpLogFile(resourceFile.getFile())
                .setMaxLineNumberToProcess(Long.MAX_VALUE);       // Total Number of records in given file is : 1.569.898

        return httpLogFileReaderService.readHttpLogFileAndPopulateDatabase();

    }



}
