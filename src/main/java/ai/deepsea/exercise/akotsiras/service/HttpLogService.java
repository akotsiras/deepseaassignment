package ai.deepsea.exercise.akotsiras.service;

import ai.deepsea.exercise.akotsiras.model.repository.HttpLogRepository;
import ai.deepsea.exercise.akotsiras.views.HostRequestURIs;
import ai.deepsea.exercise.akotsiras.views.Percentage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.text.DecimalFormat;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class HttpLogService {

    private static final Logger log = LoggerFactory.getLogger(HttpLogService.class);

    @Autowired HttpLogRepository httpLogRepository;

    /**
     * 1. Το top10 των σελίδων που ζήτησαν οι χρήστες, και τον αριθμό των requests για την κάθε σελίδα.
     */
    public List<Object> getRequestCountByURITop10() {
        System.out.println("\n");
        log.info("Executing : getRequestCountByURITop10() .........");
        Instant start = Instant.now();

        List<Object> objectList = httpLogRepository.requestCountByURITop10();
        objectList.forEach(o -> System.out.println(Arrays.toString((Object[]) o)));

        log.info("Executed : getRequestCountByURITop10() in " + Duration.between(start, Instant.now()).toMillis() + " millis");
        return objectList;
    }

    /**
     * 2. Το ποσοστό των επιτυχημένων requests (οτιδήποτε με status 2xx και 3xx).
     */
    public Percentage getPercentageOfSuccessfulRequests() {
        System.out.println("\n");
        log.info("Executing : getPercentageOfSuccessfulRequests() .........");
        Instant start = Instant.now();
        List<Object> objectList = httpLogRepository.successfulAndTotalRequestCount();
        objectList.forEach(o -> System.out.println(Arrays.toString((Object[]) o)));

        Percentage percentage = new Percentage();
        percentage.setName("PercentageOfSuccessfulRequests");
        // Ok, not so elegant array manipulation but ...will keep it for now
        percentage.setPercentage(calcAndFormatPercentage((BigInteger) ((Object[])objectList.get(0))[0], (BigInteger) ((Object[])objectList.get(0))[1]));

        log.info("Executed : getPercentageOfSuccessfulRequests() in " + Duration.between(start, Instant.now()).toMillis() + " millis");
        return percentage;
    }

    /**
     * 3. Το ποσοστό των ανεπιτυχών requests (οτιδήποτε με status διαφορετικό από 2xx και 3xx).
     */
    public Percentage getPercentageOfUnSuccessfulRequests() {
        System.out.println("\n");
        log.info("Executing : getPercentageOfUnSuccessfulRequests() .........");
        Instant start = Instant.now();
        List<Object> objectList = httpLogRepository.unsuccessfulAndTotalRequestCount();
        objectList.forEach(o -> System.out.println(Arrays.toString((Object[]) o)));

        Percentage percentage = new Percentage();
        percentage.setName("PercentageOfUnSuccessfulRequests");
        // Ok, not so elegant array manipulation but ...will keep it for now
        percentage.setPercentage(calcAndFormatPercentage((BigInteger) ((Object[])objectList.get(0))[0], (BigInteger) ((Object[])objectList.get(0))[1]));

        log.info("Executed : getPercentageOfUnSuccessfulRequests() in " + Duration.between(start, Instant.now()).toMillis() + " millis");
        return percentage;
    }

    /**
     * 4. Το top10 των ανεπιτυχών requests (χωρις count)
     */
    public List<Object> getUnsuccesfulRequestsURITop10() {
        System.out.println("\n");
        log.info("Executing : getUnsuccesfulRequestsURITop10() .........");
        Instant start = Instant.now();

        List<Object> objectList = httpLogRepository.unsuccesfulRequestsURITop10();
        objectList.forEach(o -> System.out.println(o));

        log.info("Executed : getUnsuccesfulRequestsURITop10() in " + Duration.between(start, Instant.now()).toMillis() + " millis");
        return objectList;
    }

    /**
     * 5. Το top10 των hosts που έχουν κάνει τα περισσότερα requests, εμφανίζοντας και
     *    την IP διεύθυνση/domain και τον αριθμό των requests που έχουν γίνει.
     */
    public List<Object> getRequestCountByHostTop10() {
        System.out.println("\n");
        log.info("Executing : getRequestCountByHostTop10() .........");
        Instant start = Instant.now();

        List<Object> objectList = httpLogRepository.requestCountByHostTop10();
        objectList.forEach(o -> System.out.println(Arrays.toString((Object[]) o)));

        log.info("Executed : getRequestCountByHostTop10() in " + Duration.between(start, Instant.now()).toMillis() + " millis");
        return objectList;
    }

    /**
     * 6. Για κάθε ένα από τους top10 hosts, εμφάνισε τις top5 σελίδες που ζήτησε και τον αριθμό των requests για καθεμία.
     */
    public List<HostRequestURIs> getRequestCountByHostTop10AndURITop5() {
        System.out.println("\n");
        log.info("Executing : requestCountByHostTop10AndURITop5() .........");
        Instant start = Instant.now();

        List<HostRequestURIs> hostRequestURIsList = new ArrayList<>();
        // First Retrieve the TOP 10 Hosts only
        List<Object> objectList = httpLogRepository.requestCountByHostTop10();
        objectList.stream()
                // Extract Host from first array element
                // .map(objectArray -> (String)((Object[])objectArray)[0])
                .forEach(objectArray -> {
                    String host = (String)((Object[])objectArray)[0];
                    BigInteger hostRequestCount = (BigInteger) ((Object[])objectArray)[1];
                    HostRequestURIs hostRequestURIs = new HostRequestURIs(host, hostRequestCount);
                    // For each host retrieve the TOP 5 Request URIs with the Request Count
                     List<Object> objectArrayList1 = httpLogRepository.requestCountByHostAndURITop5(host);
                     objectArrayList1.stream().forEach(
                             objectArray1 -> {
                                 String host1 = (String) ((Object[])objectArray1)[0]; // Not Used
                                 String uri = (String) ((Object[])objectArray1)[1];
                                 BigInteger count = (BigInteger) ((Object[])objectArray1)[2];
                                 hostRequestURIs.addRequestUri(uri, count);
                             }
                     );
                     hostRequestURIsList.add(hostRequestURIs);
                   }
                );

        hostRequestURIsList.forEach(hostRequestURIs -> System.out.println(hostRequestURIs));

        log.info("Executed : requestCountByHostTop10AndURITop5() in " + Duration.between(start, Instant.now()).toMillis() + " millis");
        return hostRequestURIsList;
    }



    /**
     *   Only for Internal testing. Not exposed to the {@link ai.deepsea.exercise.akotsiras.controller.HttpLogController}
     */
    public List<Object> getRequestCountByHostAndURITop5(String host) {
        List<Object> objectList = httpLogRepository.requestCountByHostAndURITop5(host);
        objectList.forEach(o -> System.out.println(Arrays.toString((Object[]) o)));
        return objectList;
    }

    private static String calcAndFormatPercentage(BigInteger numerator, BigInteger denominator) {
        DecimalFormat decimalFormat = new DecimalFormat("#.##");
        //return Float.valueOf(decimalFormat.format((numerator.floatValue()/denominator.floatValue()) * 100F));
        return decimalFormat.format((numerator.floatValue()/denominator.floatValue()) * 100F);
    }


}
