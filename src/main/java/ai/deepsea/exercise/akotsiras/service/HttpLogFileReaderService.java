package ai.deepsea.exercise.akotsiras.service;

import ai.deepsea.exercise.akotsiras.views.InitDatabaseResults;
import ai.deepsea.exercise.akotsiras.helpers.HttpLogLineParser;
import ai.deepsea.exercise.akotsiras.helpers.HttpLogLine;
import ai.deepsea.exercise.akotsiras.model.entity.HttpLog;
import ai.deepsea.exercise.akotsiras.model.entity.HttpLogFailed;
import ai.deepsea.exercise.akotsiras.model.repository.HttpLogFailedRepository;
import ai.deepsea.exercise.akotsiras.model.repository.HttpLogRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.*;
import java.time.Duration;
import java.time.Instant;

@Service
public class HttpLogFileReaderService {

    Logger log = LoggerFactory.getLogger(HttpLogFileReaderService.class);

    @Autowired HttpLogRepository httpLogRepository;
    @Autowired HttpLogFailedRepository httpLogFailedRepository;

    private File httpLogFile;
    private long maxLineNumberToProcess = Long.MAX_VALUE;    // 1569898

    // =========================================================
    public HttpLogFileReaderService() {
    }

    public HttpLogFileReaderService setHttpLogFile(File httpLogFile) {
        this.httpLogFile = httpLogFile; return this;
    }

    public HttpLogFileReaderService setMaxLineNumberToProcess(long maxLineNumberToProcess) {
        this.maxLineNumberToProcess = maxLineNumberToProcess; return this;
    }

    public InitDatabaseResults readHttpLogFileAndPopulateDatabase()  {
        InitDatabaseResults initDatabaseResults = new InitDatabaseResults();
        LineNumberReader lineNumberReader = null;
        Instant start = Instant.now();
        try {
            lineNumberReader =
                    new LineNumberReader(
                            new BufferedReader(
                                    new FileReader(httpLogFile)));

            String line;
            long lineNumber = 0;
            while (lineNumber < maxLineNumberToProcess) {
                try {
                    line = lineNumberReader.readLine();
                    lineNumber = lineNumberReader.getLineNumber();
                    if (isEmptyLine(line))  {
                        System.out.println(line);
                        log.info("############  ENF OF FILE ON LINE : " + (lineNumber+1) + " ###############");
                        return initDatabaseResults;
                    }
                    // log.info(lineNumber + " -> [" + line+ "]");
                    HttpLogLine httpLogLine = parseLine(line, lineNumber);
                    if(httpLogLine.isValid() && !httpLogLine.hasErrors()) {
                        httpLogRepository.save(new HttpLog(httpLogLine));
                        initDatabaseResults.incLogLinesCountSuccess();
                        System.out.println("OK -> " + httpLogLine.getLineNumber());
                    } else {
                        httpLogFailedRepository.save(new HttpLogFailed(httpLogLine));
                        initDatabaseResults.incLogLinesCountUnparseable();
                        System.out.println("INVALID -> " + httpLogLine.getLineNumber());
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } finally {
            try {
                if(lineNumberReader!=null)
                    lineNumberReader.close();
            } catch (IOException e) {
                log.error("ERROR: ", e);
            }
            initDatabaseResults.setExecutionTimeSeconds( Duration.between(start, Instant.now()).toSeconds());
            return initDatabaseResults;
        }
    }

    HttpLogLine parseLine(String line, long lineNumber)  {
        HttpLogLineParser httpLogLineParser = new HttpLogLineParser(line, lineNumber);
        HttpLogLine httpLogLine = httpLogLineParser.parseLine();
        return httpLogLine;
    }

    private static boolean isEmptyLine(String line) {
        return line==null || line.isBlank();
    }


}
