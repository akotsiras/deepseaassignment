package ai.deepsea.exercise.akotsiras.config;

import ai.deepsea.exercise.akotsiras.service.HttpLogService;
import ai.deepsea.exercise.akotsiras.service.InitDatabaseService;
import ai.deepsea.exercise.akotsiras.views.InitDatabaseResults;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

@Configuration
public class AppConfig {
    private static final Logger log = LoggerFactory.getLogger(AppConfig.class);

    // Not Used
    @Autowired private Environment env;
    @Autowired private ApplicationContext context;

    @Autowired InitDatabaseService initDatabaseService;
    @Autowired HttpLogService httpLogService;


    // *****************************  Custom application.properties  **************************
    private boolean populateDatabaseOnStartup;
    private boolean executeHttpLogServiceCallsOnStartup;

    @Value("${ai.deepsea.populateDatabaseOnStartup:FALSE}")
    public void setPopulateDatabaseOnStartup(String populateDatabaseOnStartup) {
        this.populateDatabaseOnStartup = Boolean.parseBoolean(populateDatabaseOnStartup.trim().toLowerCase());
    }

    @Value("${ai.deepsea.executeHttpLogServiceCallsOnStartup:FALSE}")
    public void setExecuteHttpLogServiceCallsOnStartup(String executeHttpLogServiceCallsOnStartup) {
        this.executeHttpLogServiceCallsOnStartup = Boolean.parseBoolean(executeHttpLogServiceCallsOnStartup.trim().toLowerCase());
    }
    // ****************************************************************************************


    /**
     * !!!! This is entry Point of the Application that is being executed upon Startup !!!
     */
    @Bean
    CommandLineRunner initApplication() {
        return args -> {

            log.info("******************     InitApplication() START     ***********************");
            /**
             * Populate the HTTP_LOG and HTTP_LOG_FAILED tables from the contents of the Http Log file provided ny DeepSea
             * That can ideally executed just once to populate the tables
             */
            if(populateDatabaseOnStartup) {
                InitDatabaseResults initDatabaseResults = initDatabaseService.populateDatabase();
                System.out.println("Valid Lines       : " + initDatabaseResults.getLogLinesCountSuccess());
                System.out.println("Unparseable Lines : " + initDatabaseResults.getLogLinesCountUnparseable());
                System.out.println("Total Lines       : " + initDatabaseResults.getLogLinesTotal());
            }

            /** Option to execute all of the specified DeepSea 6 Calls upon startup and have an immediate feedback on the Log File (Console) of the results returned for each Call.
                Not via the Controller but via the internal {@link HttpLogService} service class
             */
            if(executeHttpLogServiceCallsOnStartup) {
                /* 1.) */   httpLogService.getRequestCountByURITop10();
                /* 2.) */   httpLogService.getPercentageOfSuccessfulRequests();
                /* 3.) */   httpLogService.getPercentageOfUnSuccessfulRequests();
                /* 4.) */   httpLogService.getUnsuccesfulRequestsURITop10();
                /* 5.) */   httpLogService.getRequestCountByHostTop10();
                /* 6.) */   httpLogService.getRequestCountByHostTop10AndURITop5();
            }

            log.info("******************     InitApplication() END     ***********************");
        };
    }
}
