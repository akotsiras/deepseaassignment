package ai.deepsea.exercise.akotsiras.helpers;

import java.util.ArrayList;
import java.util.List;

/**
 * Utility Java Bean to Store the fields of each Parsed Http Log Line.
 * Also Holds info regarding the Validity of the Line and possible errors that where found during the parsing and tokenizing of the line
 */
public class HttpLogLine {

    /**
     *  If TRUE the Log Line will be saved into HTTP_LOG table.
     *  If FALSE the Log Line will be saved into the HTTP_LOG_FAILED table
     */
    boolean isValid = true;

    /**
     *  If {@link isValid == true} then there should be errors.
     *  The list of Errors bellow will be saved into the HTTP_LOG_FAILED.ERRORS column in Pipe Delimited format
     */
    List<String> errors = new ArrayList<>();

    private String line;
    private long lineNumber;

    String clientHost = null;
    String requestTime = null;
    long requestepoch;
    String clientRequest = null;
    int httpStatusCode;
    String numOfBytes = null;

    public HttpLogLine(String line, long lineNumber) {
        this.line = line;
        this.lineNumber = lineNumber;
    }

    public String getClientHost() {
        return clientHost;
    }

    public void setClientHost(String clientHost) {
        this.clientHost = clientHost;
    }

    public String getRequestTime() {
        return requestTime;
    }

    public void setRequestTime(String requestTime) {
        this.requestTime = requestTime;
    }

    public String getClientRequest() {
        return clientRequest;
    }

    public void setClientRequest(String clientRequest) {
        this.clientRequest = clientRequest;
    }

    public int getHttpStatusCode() {
        return httpStatusCode;
    }

    public void setHttpStatusCode(int httpStatusCode) {
        this.httpStatusCode = httpStatusCode;
    }

    public String getNumOfBytes() {
        return numOfBytes;
    }

    public void setNumOfBytes(String numOfBytes) {
        this.numOfBytes = numOfBytes;
    }

    public long getRequestepoch() {
        return requestepoch;
    }

    public void setRequestepoch(long requestepoch) {
        this.requestepoch = requestepoch;
    }

    public boolean isValid() {
        return isValid;
    }

    public void setValid(boolean valid) {
        isValid = valid;
    }

    public List<String> getErrors() {
        return errors;
    }

    public void addError(String error) {
        this.errors.add(error);
    }

    public String getLine() {
        return line;
    }

    public long getLineNumber() {
        return lineNumber;
    }

    public boolean hasErrors() {
        return !this.errors.isEmpty();
    }

    @Override
    public String toString() {
        return "HttpLogLine{" +
                "lineNumber=" + lineNumber +
                ", clientHost='" + clientHost + '\'' +
                ", requestTime='" + requestTime + '\'' +
                ", requestepoch=" + requestepoch +
                ", clientRequest='" + clientRequest + '\'' +
                ", httpStatusCode=" + httpStatusCode +
                '}';
    }
}
