package ai.deepsea.exercise.akotsiras.helpers;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Performs the parsing and tokenizing of each Line of the Http log File
 */
public class HttpLogLineParser {

    static boolean DEBUG = true;
    static final SimpleDateFormat ACCESSLOG_DATE_FORMAT = new SimpleDateFormat("dd/MMM/yyyy:HH:mm:ss Z", Locale.US);


    private String line;
    private long lineNumber;

    public HttpLogLineParser(String line, long lineNumber) {
        this.line = line;
        this.lineNumber = lineNumber;
    }


    /**
     * Parses, tokenizes, validates and finally constructs an {@link HttpLogLine} object for each line of the Http Log file
     * Plenty of validation is implemented but not every possible scenario.
     * 95% i belived there should be invalid formats that i have not addressed )) ..its ok
     * @return {@link HttpLogLine}
     */
    public HttpLogLine parseLine()  {

        HttpLogLine logLine = new HttpLogLine(line, lineNumber) ;

        // Extract Host : It's the substring from start till the "- -" literal, e.g. -> 140.171.97.13 - -
        if(line.indexOf("- -")  > 0) {
            logLine.setClientHost(line.substring(0,line.indexOf("- -")).trim());
        } else {
            logLine.setValid(false);
            logLine.addError("Could not parse ClientHost. No \"- -\" symbol");
        }

        // Extract Request Timestamp : It's the substring between the opening and closing brackets, e.g. -> [08/Aug/1995:12:17:11 -0400]
        if(line.indexOf("[") > 0 && line.indexOf("]") > 0) {
            String requestTimestamp = line.substring(line.indexOf("[")+1, line.indexOf("]"));
            logLine.setRequestTime(requestTimestamp.trim());
            try {
                //todo
                // Convert request time with Time Zone String to Epoch
                // Well ...finally the conversion bellow does not seem to be correct - need to deal with ZonedDateTime- but anyways, that field is not being used by the App
                logLine.setRequestepoch(convertTimetoEpoch(requestTimestamp.trim()));
            } catch (ParseException e) {
                e.printStackTrace();
                logLine.setValid(false);
                logLine.addError("Could not parse Request Timestamp: " + e);
            }
        } else {
            logLine.setValid(false);
            logLine.addError("Could not parse Request Timestamp: No opening or closing brackets \"[\",\"]\"");
        }

        // Extract Request URI : Retrieve the substring between the first and the last double Quotes, e.g. -> "GET /shuttle/countdown/video/livevideo.jpeg HTTP/1.0"
        // Split it into an array and read the second element of the array that will be the URI,  e.g. -> /shuttle/countdown/video/livevideo.jpeg
        // Request method is always GET and i decided that it's of no use
        // Protocol name and version HTTP/1.0 and it's of no use as well
        int firstDoubleQuoteIndex  = line.indexOf("\"");
        int secondDoubleQuoteIndex = line.lastIndexOf("\"");
        if(firstDoubleQuoteIndex > 0 && secondDoubleQuoteIndex > 0 && firstDoubleQuoteIndex < secondDoubleQuoteIndex) {
            String methodAndUriAndProtocol = line.substring(firstDoubleQuoteIndex+1, secondDoubleQuoteIndex);
            // Will split the string into a string array with separator as space or multiple spaces.
            String[] methodAndUriAndProtocolArray = methodAndUriAndProtocol.split("\\s+");
            // SOS !  "HTTP/1.0" might be missing. Array length will be 2 in that case instead of 3. Does not seem to be a problem though
            if(methodAndUriAndProtocolArray == null || methodAndUriAndProtocolArray.length < 2 ) {
                logLine.setValid(false);
                logLine.addError("Could not extract URI from \"Method URI Protocol\" literal: \"" + methodAndUriAndProtocol + "\"");
            } else {
                String uri = methodAndUriAndProtocolArray[1]; // Second Array Element is the URI. That's all i need
                logLine.setClientRequest(uri.trim());
            }
        } else {
            logLine.setValid(false);
            logLine.addError("Could not parse Method and URI: Missing or invalid double quote symbols");
        }

        // Extract Status Code and Bytes : It's the substring starting at the second double quote till the end of the line, e.g. -> 200 46053
        if(secondDoubleQuoteIndex > 0) {
            String statusCodeAndBytes  = line.substring(secondDoubleQuoteIndex+1);
            if(statusCodeAndBytes == null || statusCodeAndBytes.trim().length() == 0) {
                logLine.setValid(false);
                logLine.addError("Could not parse status code: \"" + statusCodeAndBytes + "\"");
            }
            String[] statusCodeAndBytesArray = statusCodeAndBytes.trim().split("\\s+");   // always trim() !!
            if(statusCodeAndBytesArray == null || statusCodeAndBytesArray.length != 2) {
                logLine.setValid(false);
                logLine.addError("Could not parse status code: \"" + statusCodeAndBytes + "\"");
            } else {
                try {
                    int iStatusCode = Integer.parseInt(statusCodeAndBytesArray[0].trim());
                    logLine.setHttpStatusCode(iStatusCode);
                }catch (NumberFormatException e) {
                    logLine.setValid(false);
                    logLine.addError("Could not parse status code: \"" + statusCodeAndBytesArray[0].trim() + "\". Not an Integer");
                }
                logLine.setNumOfBytes(statusCodeAndBytesArray[1].trim());
            }
        }

        return logLine;
    }

    /**
     * Converts any formated date to epoch
     *@param date : it should fit dateFormmat criteria
     *@return long : epoch representation
     *@throws ParseException
     */
    private static long convertTimetoEpoch(String date) throws ParseException
    {
        return (ACCESSLOG_DATE_FORMAT.parse(date)).getTime();
    }




     // #####################  initial Regex based approach. Did not work and was abandoned   ###################################

    static final Pattern ACCESS_LOG_PATTERN = Pattern.compile(getHttpLogRegex(),Pattern.CASE_INSENSITIVE | Pattern.DOTALL);

    /**
     * Structures Apache combined access log
     *@return regex
     */
    private static String getHttpLogRegex()
    {
        String regex1 = "^([\\S+]+)";                         // Client IP
        String regex2 = " (\\S+)";                            // -
        String regex3 = " (\\S+)";                            // -
        String regex4 = " \\[([\\w:/]+\\s[+\\-]\\d{6})]";   // Date
        String regex5 = " \"(.+?)\"";                         // request method and url
        String regex6 = " (\\d{3})";                          // HTTP code
        String regex7 = " (\\d+|(.+?))";                      // Number of bytes
        return regex1+regex2+regex3+regex4+regex5+regex6+regex7;
    }

    /**
     *  Initial quick and dirty solution to use a RegEx utility that was  found on the net. Did not work ...big time !!
     *  Plus the fact that there would be no control on the Error Reason reporting, meaning which part of the line is not valid and cannot be parsed
     *  https://gist.github.com/johnlcox/6063613
     */
    public HttpLogLine parseLineViaRegex()  {

        HttpLogLine httpLogLine = new HttpLogLine(line, lineNumber) ;
        Matcher accessLogEntryMatcher = ACCESS_LOG_PATTERN.matcher(line);

        if(!accessLogEntryMatcher.matches())
        {
            if (DEBUG)System.out.println("[" + line +"] : couldn't be parsed");
            httpLogLine.setValid(false);
            return httpLogLine;
        }
        else
        {
            httpLogLine.setClientHost(accessLogEntryMatcher.group(1));
            String requestTime=accessLogEntryMatcher.group(4);
            httpLogLine.setClientRequest(accessLogEntryMatcher.group(5));
            httpLogLine.setHttpStatusCode(Integer.parseInt(accessLogEntryMatcher.group(6)));
            httpLogLine.setNumOfBytes(accessLogEntryMatcher.group(7));
            try {
                httpLogLine.setRequestTime(requestTime);
                httpLogLine.setRequestepoch(convertTimetoEpoch(requestTime));
            } catch (ParseException e) {
                e.printStackTrace();
                httpLogLine.setValid(false);
                httpLogLine.addError(e.toString());
                return httpLogLine;
            }
            httpLogLine.setValid(true);
            return httpLogLine;
        }
    }

}
