package ai.deepsea.exercise.akotsiras;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AkotsirasApplication {

	/**
	 * Spring Boot Application main method
	 * The Startup logic though in not defined here but instead in the {@link ai.deepsea.exercise.akotsiras.config.AppConfig#initApplication} class method
	 */

	public static void main(String[] args) {
		SpringApplication.run(AkotsirasApplication.class, args);
	}

}
