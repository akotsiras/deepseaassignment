package ai.deepsea.exercise.akotsiras;

import ai.deepsea.exercise.akotsiras.controller.HttpLogController;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.PostConstruct;

/** @see <a href="https://spring.io/guides/gs/testing-web/">Testing the Web Layer</a>
 *
 *  Tells Spring Boot to look for a main configuration class (one with @SpringBootApplication, for instance) and use that to start a Spring application context.
 *  You can run this test in your IDE or on the command line
 *  A nice feature of @SpringBootTest is that the application context is cached between tests. That way, if you have multiple methods in a test case or multiple test cases with the same configuration,
 *  they incur the cost of starting the application only once
 */
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class AkotsirasApplicationTests {

	@LocalServerPort
	private int port;

	@Autowired
	private TestRestTemplate restTemplate;

	// The controller is injected before the test methods are run (Not really used, just to verify successful injection)
	@Autowired
	private HttpLogController httpLogController;


	@PostConstruct
	void init() {
		System.out.println("\n\n############################################################################");
		System.out.println(this.getClass().getSimpleName() + ".init()");
	}

	@Test
	void contextLoads() {
		System.out.println("Just Make sure the HttpLogController is OK");
		System.out.println(HttpLogController.class.getSimpleName() + " initialized at : " + this.httpLogController.getInitializedDateTime());
	}

	// ##########################################  Integration test methods START ###################################

	/** 1. Το top10 των σελίδων που ζήτησαν οι χρήστες, και τον αριθμό των requests για την κάθε σελίδα. */
	@Test
	public void getRequestCountByURITop10() throws Exception {
		String response = this.restTemplate.getForObject(getRestUrlForMethod("requestCountByURITop10"), String.class);
		System.out.println(prettyPrintJSON(response));
	}

	/** 2. Το ποσοστό των επιτυχημένων requests (οτιδήποτε με status 2xx και 3xx). */
	@Test
	public void getPercentageOfSuccessfulRequests() throws Exception {
		String response = this.restTemplate.getForObject(getRestUrlForMethod("percentageOfSuccessfulRequests"), String.class);
		System.out.println(prettyPrintJSON(response));
	}

	/** 3. Το ποσοστό των ανεπιτυχών requests (οτιδήποτε με status διαφορετικό από 2xx και 3xx). */
	@Test
	public void getPercentageOfUnSuccessfulRequests() {
		String response = this.restTemplate.getForObject(getRestUrlForMethod("percentageOfUnSuccessfulRequests"), String.class);
		System.out.println(response);
	}

	/** 4. Το top10 των ανεπιτυχών requests (χωρις count) */
	@Test
	public void getUnsuccesfulRequestsURITop10() throws Exception {
		String response = this.restTemplate.getForObject(getRestUrlForMethod("unsuccesfulRequestsURITop10"), String.class);
		System.out.println(prettyPrintJSON(response));
	}

	/** 5. Το top10 των hosts που έχουν κάνει τα περισσότερα requests, εμφανίζοντας και την IP διεύθυνση/domain και τον αριθμό των requests που έχουν γίνει. */
	@Test
	public void getRequestCountByHostTop10() throws Exception {
		String response = this.restTemplate.getForObject(getRestUrlForMethod("requestCountByHostTop10"), String.class);
		System.out.println(prettyPrintJSON(response));
	}

	/** 6. Για κάθε ένα από τους top10 hosts, εμφάνισε τις top5 σελίδες που ζήτησε και τον αριθμό των requests για καθεμία. */
	@Test
	public void getRequestCountByHostTop10AndURITop5() throws Exception {
		String response = this.restTemplate.getForObject(getRestUrlForMethod("requestCountByHostTop10AndURITop5"), String.class);
		System.out.println(prettyPrintJSON(response));
	}

	/** Όλα μαζί ! */
	@Test
	public void invokeAllRestCalls() throws Exception {
		getRequestCountByURITop10();
		getPercentageOfSuccessfulRequests();
		getPercentageOfUnSuccessfulRequests();
		getUnsuccesfulRequestsURITop10();
		getRequestCountByHostTop10();
		getRequestCountByHostTop10AndURITop5();
	}
	// ##########################################  Integration test methods END ###################################


	private String getRestUrlForMethod(String methodRequestMapping) {
		/* 	 /api/v1/httplog   */
		String baseHttplogControllerMapping  = httpLogController.getClass().getAnnotation(RequestMapping.class).path()[0];

		return
				new StringBuilder("http://localhost:").append(port).append("/")
						.append(baseHttplogControllerMapping).append("/")
						.append(methodRequestMapping)
						.toString();

	}

	private static String prettyPrintJSON(String uglyJson) throws JsonProcessingException {
		if(uglyJson == null || uglyJson.isEmpty()) return uglyJson;
		return new ObjectMapper()
				.enable(SerializationFeature.INDENT_OUTPUT)
				.writerWithDefaultPrettyPrinter()
				.writeValueAsString(uglyJson);
	}

}
